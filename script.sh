#!/bin/bash

MAX_DATE_first="0"
MAX_DATE_second="0"
max_date_path_first="0"
max_date_path_second="0"
CURRENT_TIME=$(python3 time_check.py)
echo "CURRENT: $CURRENT_TIME"

function find_two_recent_files_from_date { #Finding 2 most recent files
    if [ "$1" -ge $MAX_DATE_first ]; then
        MAX_DATE_second=$MAX_DATE_first
        max_date_path_second=$max_date_path_first
        MAX_DATE_first=$1
        return "1"
    else
        if [ "$1" -ge $MAX_DATE_second ]; then
            MAX_DATE_second=$1
            return "2"
        fi
    fi
}

function get_date_time_from_full_path { #Param: full path to file. Return: 1 if date is valid
    fullfile=$1
    temp="${fullfile##*/}" #Get only filename
    date=${temp:11:4}${temp:15:2}${temp:17:2}${temp:20:2}${temp:22:2}${temp:24:2} #Gets date and time in special format
    if [ "$date" -gt "$CURRENT_TIME" ]; then                                      #Detecting "files from future"
        return 0
    fi
    result=$(python3 time_check.py "$date") #Validating data
    if [ "$result" == "1" ]; then
        find_two_recent_files_from_date "$date"
        if [ $? == "1" ]; then
            max_date_path_first=$temp
        else
            max_date_path_second=$temp
        fi
    fi
    return "$result"
}
echo "Deleted:"
if [ $# == 0 ]; then #exit if there are no arguments
    exit
fi
for item in "$1"/*; do
    if [[ $item =~ Partition[01]\.[0-9]{8}T[0-9]{6}\.verified\.gz ]]; then #Check template
        get_date_time_from_full_path "$item"
        if [ $? != "1" ]; then #Validate date
            echo "$item - invalid date"
            rm -rf "$item"
        fi
    else
        echo "$item" && rm -rf "$item"
    fi
done
echo "Most recent: $max_date_path_first and $max_date_path_second"
for item in "$1"/*; do
    fullfile=$item
    temp="${fullfile##*/}"
    if [ "$temp" != "$max_date_path_first" ] && [ "$temp" != "$max_date_path_second" ]; then #Delete old files
        echo "$item - old" 
        rm -rf "$item" 
    fi
done
