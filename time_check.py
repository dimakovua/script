
import time
import datetime
import sys

if len(sys.argv)==1: #Get current date and time
    now = datetime.datetime.now()
    print (now.strftime("%Y%m%d%H%M%S"))

else:
    datetime_str = sys.argv[1]
    try:
        datetime_object = time.strptime(datetime_str, '%Y%m%d%H%M%S') #Validating date
    except ValueError as e:
        print(0)
        exit(0)
    print(1)
    exit(1)

