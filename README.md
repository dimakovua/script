Create a bash script, which will take as a parameter the path to the directory, 
and remove from this directory all the content including subdirectories except 
for the two most recent archive files, whose name corresponds to the specified pattern.
The date and time of creation of the archive file must be obtained from the filename of the archive. 
You must also log the deletion of files and subdirectories by displaying the name of the deleted object in the console.

The pattern of the archive file name:
PartitionN.YYYYMMDDTHHMMSS.verified.gz

Description of the parts of the name in the template:
Partition - the immutable part of the name;
N - partition number, one character, numeric value 0 or 1;
. - dot character, separator, immutable part of the name;
YYYYMMDD - date of archive file creation in the format YYYYMMDD, 8 characters, digits only;
T - character "T", separator, immutable part of the name;
HHMMSS - the archive file creation time in the HHMMSS format, 6 characters, digits only;
.verified.gz - the immutable part of the name;

The pattern of the archive file name with the wildcard characters:
Partition?.????????T??????.verified.gz

Examples of the correct name of the archive file:
Partition0.20181108T091005.verified.gz
Partition1.20200812T132937.verified.gz
